
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Opening code

function varargout = training_game_new(varargin)
% TRAINING_GAME_NEW MATLAB code for training_game_new.fig
%      TRAINING_GAME_NEW, by itself, creates a new TRAINING_GAME_NEW or raises the existing
%      singleton*.
%
%      H = TRAINING_GAME_NEW returns the handle to a new TRAINING_GAME_NEW or the handle to
%      the existing singleton*.
%
%      TRAINING_GAME_NEW('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in TRAINING_GAME_NEW.M with the given input arguments.
%
%      TRAINING_GAME_NEW('Property','Value',...) creates a new TRAINING_GAME_NEW or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before training_game_new_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to training_game_new_OpeningFcn via
% %      varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help training_game_new

% Last Modified by GUIDE v2.5 24-Mar-2016 15:02:59

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @training_game_new_OpeningFcn, ...
    'gui_OutputFcn',  @training_game_new_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%CREATE IMPORTENT VARIABLES

% --- Executes just before training_game_new is made visible.
function training_game_new_OpeningFcn(hObject,eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to training_game_new (see VARARGIN)

% Choose default command line output for training_game_new
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes training_game_new wait for user response (see UIRESUME)
% uiwait(handles.figure1);
global start_time;
global modelName
global thresh
global arrow_amp
%  global pause_flag
global num_above_tresh
global isdone
global val2_before
global val3_before
global IAF_range
global aud_stim
global fs
global indiv
global Averge_pow_freq
global EOG_view
global old_timeind
global old_timeind2
global old_timeind3
global address
global rec_tb
global dex1
global dex2
global HEOG_mat
global VEOG_mat
global amplitude_mat1
global amplitude_mat2
global win_target_sound
global elec_name
global artifact_on
global Break_on
global sound_on
global cutoff
global finish
global pause_dex
global index_t
global subject_details_on
global counter_right
global u
global u2
global pause_game
global amplitude_mat
global Sample_time
global Elec_num
global timer2
global Timer_on
global Sound_time
global Scount
global featureVec
global surprise
global  minTrialsNumforNF
global soundSequence
global stimulusInfo
global sequenceData
global counter
global featureInfo
global pastLength
global allBeeps
global timer3
global pahandle

counter=0;
Sample_time=1; %the time choose to take for calculate the feature
Elec_num=10; % the electrodes chosen for calculating the feature
fs=256;
Sound_time= 1.2; %create sound in the game every Sound time seconds...

timer3=0;
timer2=0;
Timer_on=0;
featureVec=[];
cutoff= 10*fs; %start time for cleaning the noise

addpath C:\Users\hadarl\Dropbox\Oren_Hadar\NF_software_P300_Surprise\io64

 %connect to port config_io;
    global cogent;
 % LPT port address
%     address =  53264;
    address =  53504;

beepDuration= 0.125;
fsBeep=44100;
freq1 = 1263;
freq2 = 1000;
intensity = 0.3;
beep1= MakeBeep(freq1,beepDuration,fsBeep)*intensity; % high frequency, high intensity
beep2= MakeBeep(freq2,beepDuration,fsBeep)*intensity; % low  frequency, high intensity
allBeeps = [beep1; beep2];

% % % InitializePsychSound;
% % % pahandle=PsychPortAudio('open',[],[],[],fsBeep,1);



%initiate the trigger sending to unity

% u= udp( '127.0.0.1' ,27643);  %ip of the current computer
% u= udp( '132.73.192.193' ,27643);  %ip of the second computer
%u= udp( '132.72.154.143' ,27643);  %ip of the second computer
% u= udp( '132.72.155.212' ,27643);  %ip of the second computer
% u= udp( '132.72.153.143' ,27643);  %ip of the second computer
% u = udp('132.65.38.25' ,27643);  
u = udp('10.0.0.1' ,27640);  
u2 = udp('10.0.0.1' ,27643);

fopen(u);
fopen(u2);



pause_game=-300; % the sign to pause


% if (~exist('hWnd', 'var'))
%     hWnd = double(0);
% end
% messageId = double(1);

%Hadar functions and parameters:
% addpath C:\Users\hadarl\Dropbox\Oren_Hadar\surprise_features
addpath ..\surprise_features
%create sound sequence- 0= oddball 1= standard
[sequenceData, stimulusInfo] = generate_sequence();

soundSequence= sequenceData.seq1; %sequence vector

%soundFreq=stimulusInfo.freq; %the first- oddball, second- standard
Scount=1; %prepare sound counter
%calibretion parameters
%for testing

featureInfoFile = '..\surprise_features\featureInfo_HLA_25-Apr-2016_s2.mat';
featureInfo = load(featureInfoFile);
featureInfo = featureInfo.featureInfo;
surprise = get_surprise_model(sequenceData,featureInfo);
handles.userdata.surprise = surprise;

pastLength = featureInfo.modelParams.pastLength;
betaInd = featureInfo.modelParams.betaInd;
% surprise = squeeze(surpriseModels(pastLength,betaInd,:));

minTrialsNumforNF = pastLength*2+1;

counter_right=0;

Break_on= 0;

artifact_on=0;
finish=0;
elec_name={'VEOG1','pz', 'VEOG2', 'HEOG1', 'HEOG2'};

sound_on=1;

% Initiate the triger sending

%connect to port
% config_io;
% % optional step: verify that the inpoutx64 driver was successfully initialized
% global cogent;
% if( cogent.io.status ~= 0 )
%     error('inp/outp installation failed');
% end
% % LPT port address
% address =  53264;
subject_details_on=0;
HEOG_mat=zeros(1,100);
VEOG_mat= zeros(1,100);
amplitude_mat1= zeros(Elec_num,round(fs* Sample_time));
%%amplitude_mat2= zeros(1,round(fs* 3));
%%amplitude_mat= zeros(1,round(fs* 3));
pause_dex=1/fs;
dex1=1;
dex2=1;
rec_tb=1;
old_timeind=0;
old_timeind2=0;
old_timeind3=0;
EOG_view=0;
IAF_range=0;
index_t=1;
num_above_tresh=0;
isdone=0;
arrow_amp=0;
indiv=0;
Averge_pow_freq=0;
thresh= 0.1;

val2_before=0;
val3_before=0;
modelName = 'ERP_NF_huji';
start_time= cutoff/fs;


%OPENING THE GUI

% create an axes that spans the whole gui
ah1 = axes('unit', 'normalized', 'position', [0 0 1 1]);
% import the background image and show it on the axes
bg = imread('images/bg.jpg'); imagesc(bg);
% prevent plotting over the background and turn the axis off
set(ah1,'handlevisibility','off','visible','off')
% making sure the background is behind all the other uicontrols
uistack(ah1, 'bottom');

% --- Outputs from this function are returned to the command line.
function varargout = training_game_new_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% CALLBACKs IN THE GUI - the buttons


function editGameDuration_Callback(hObject, eventdata, handles)
% hObject    handle to editGameDuration (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editGameDuration as text
%        str2double(get(hObject,'String')) returns contents of editGameDuration as a double


% --- Executes during object creation, after setting all properties.
function editGameDuration_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editGameDuration (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function editNumBreaks_Callback(hObject, eventdata, handles)
% hObject    handle to editNumBreaks (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editNumBreaks as text
%        str2double(get(hObject,'String')) returns contents of editNumBreaks as a double


% --- Executes during object creation, after setting all properties.
function editNumBreaks_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editNumBreaks (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editName_Callback(hObject, eventdata, handles)
% hObject    handle to editName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editName as text
%        str2double(get(hObject,'String')) returns contents of editName as a double


% --- Executes during object creation, after setting all properties.
function editName_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%PAUSING THE GAME

function pause_game()

global set_sound
global address
global Break_on
% global modelName
global time_breaks
global actime
global rec_tb
global fig5
global counter_right
global score_per_round
global index6
global pause_game
global u
global u2
Break_on= 1 ;
set_sound=0;
index6= index6 +1;
score_per_round(index6)= counter_right;
time_breaks(1,rec_tb)= actime; %record the start time and end time of each break for later deleting this sections from the data.

d = createMessage(pause_game,1);
fwrite(u,d);
fwrite(u2,d);

pause_game=pause_game -1;

% fig5=figure('Toolbar','none','Menubar','none','NumberTitle','off',...
%     'Units','normalized','position',[0 0 1 1],'Color',[1 1 1]);
%
% % Display some text on the figure:
% text(0.3,0.6,['Round ' num2str(index6) ' is over!'], 'fontsize', 50);
% text(0.23,0.45,'Take a few seconds break and prepare for the next round.', 'fontsize', 20);
% text(-0.1,0.9, ['Round score: ' num2str(score_per_round(index6))], 'fontsize', 45, 'color', 'b');
%
% axis off

function close_pause()

global set_sound
global address
global Break_on
% global modelName
global time_breaks
global actime
global rec_tb
global fig5
global counter_right
global pause_game
global u
global u2

counter_right=0;
set_sound=1;
%close(fig5)
Break_on=0;

d = createMessage(-200,1); % back to the game
fwrite(u,d);
fwrite(u2,d);

pause_game=-300;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%OPEN THE SIMULINK RECORDING

function LoadModel()

global modelName
global Scope
global OriginalStopTime
global OriginalMode
global OriginalStartFcn

if isempty(find_system('Type','block_diagram','Name',modelName)),
    load_system(modelName);
end

% Store model's parameters, in order to restore them in the end
OriginalStopTime = get_param(modelName,'Stoptime');
OriginalMode = get_param(modelName,'SimulationMode');
OriginalStartFcn = get_param(modelName,'StartFcn');

% Add a listener to the 'Scope' block

% Add a listener to the 'Scope' block


Scope = struct(...
    'blockName','',...
    'blockHandle',[],...
    'blockEvent','',...
    'blockFcn',[]);

Scope.blockName = sprintf('%s/Scope',modelName);
Scope.blockHandle = get_param(Scope.blockName,'Handle');
Scope.blockEvent = 'PostOutputs';
Scope.blockFcn = @ScopeCallback;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%PUSHBUTTONS=  STARTING AND FINISHING THE GAME


%START BUTTON

function pushbuttonStart_Callback(hObject, eventdata, handles)
% hObject    handle to pushbuttonStart (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Scope
global eventHandle
global TimeInd
global modelName
global isdone
global target
global  Session_Time
global low_feedback_thresh
global max_feddback_thresh
global N_breaks
global HEOG_thresh
global VEOG_thresh
global IAF_range
global LTF
global HTF
global thresh
global indiv
global Averge_pow_freq
global breaks
global set_sound
global time_breaks
global b_low
global b_high
global Break_time
global thresh_change
global band_range
global Timer
global score_per_round
global index6
global start_time;
global MA_dex
global smothing_level
global MAT_vals
global u
global u2
global first_thresh
global Subject_Name
global Scount
global featureInfo
global featureVec
% load(featureInfoFile)
% load('C:\Users\hadarl\Dropbox\Oren_Hadar\surprise_features\featureInfo.mat')

%for real
%featureInfo= ?

Scount=1;
featureVec = [];

d = createMessage(-100,1);
fwrite(u,d);
fwrite(u2,d);

MA_dex=0;

index6=0;
Timer=0;
set_sound=1;
target=round(rand(1));
isdone=0;
TimeInd=0;
set(handles.pushbuttonStart,'enable','off')
set(handles.pushbuttonStop,'enable','on')
%get bands for BPF

Subject_Name=  get(handles.editName,'string'); %take subject name
Session_Time = 60*str2num(get(handles.editGameDuration,'string')); %make it in minutes by multiplying in 60
% save Session_Time Session_Time
N_breaks= str2num(get(handles.editNumBreaks,'string'));
score_per_round= zeros(1,N_breaks);

thresh_change= zeros(1, Session_Time/60);
score_per_round= zeros(1,N_breaks);

breaks= round(Session_Time/(N_breaks)); %create a time until the simulink(record) stops for a while and then continue.
%Breaks= str2double(get(handles.editNumBreaks,'string'));
time_breaks=zeros(2,N_breaks);
Break_time=15; % time of the break in seconds

LoadModel();% Load the SIMULINK model

% set the stop time to Session_Time
set_param(modelName,'StopTime',num2str(start_time + Session_Time + N_breaks*Break_time));
%set_param(modelName,'StopTime','inf');

% set the simulation mode to normal
set_param(modelName,'SimulationMode','normal');


% start the model
set_param(modelName,'SimulationCommand','start');

% Set a listener
eventHandle = add_exec_event_listener(Scope.blockName,Scope.blockEvent, Scope.blockFcn);





%STOP BUTTON

function pushbuttonStop_Callback(hObject, eventdata, handles)
% hObject    handle to pushbuttonStop (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global modelName
global fig
global isdone
global u
global u2
global NFparamForGraph
global featureVec
global surprise
d0=createMessage(0,1);
fwrite(u,d0)

isdone=1;
set(handles.pushbuttonStop,'enable','off')
set(handles.pushbuttonStart,'enable','on')
% stop the model
set_param(modelName,'SimulationCommand','stop');

pause(5)

figure
TP= 1: length(NFparamForGraph);
plot(TP,NFparamForGraph)
xlabel('Trial')
ylabel('Corrolation Level')

figure
TP_featureVec= 1: length(featureVec);
plot(TP_featureVec,featureVec/1000)
hold on 
plot(TP_featureVec,surprise(1:length(featureVec)),'g')
legend('EEG feature', 'surprise feature')
xlabel('Trial')
ylabel('Value')

figure
scatter(surprise(1:length(featureVec)),featureVec)


% figure(3)
% 
% TP_featureVec= 1: length(featureVec);
% plot(TP_featureVec,handles.userdata.featureVec/1000)
% hold on 
% plot(TP_featureVec,handles.userdata.surprise(1:length(handles.userdata.featureVec)),'g')
% legend('EEG feature', 'surprise feature')
% xlabel('Trial')
% ylabel('Value')
% title('userdata')
% 
save('NF_ERP_Data.mat','featureVec' ,'surprise' ,'NFparamForGraph')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% CLOSE THE NF SOFTWARE

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function pushbuttonClose_Callback(hObject, eventdata, handles)
% hObject    handle to pushbuttonClose (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% testgui2;   % ������ ��� ���� ������ �������

close training_game_new



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%----------------------------------------------------------------------------
% Callback Function for executing the event listener on the EEG Scope
%----------------------------------------------------------------------------

%THE SCOPE CALLBACK FUNCTION- entering the data into matlab in live

function ScopeCallback(block, eventdata) %#ok


global TimeInd
global start_time;
global val;
global  Session_Time
global N_breaks
global breaks
global dex1
global artifact_on
global Break_on
global actime
global amplitude_mat1
global pause_dex
global Break_time
global Timer
global cutoff
global counter_right
global u
global u2
global fs
global Sample_time
global Elec_num
global timer2
global Timer_on
global Sound_time
global Scount
global featureInfo
global featureVec
global surprise
global  minTrialsNumforNF
global soundSequence
global stimulusInfo
global sequenceData
global counter
global NFparamForGraph
global pastLength
global allBeeps
global address
global timer3
global pahandle

% Update the "Time"
Timer= Timer +1;
TimeInd = TimeInd + 1;

%actual time
actime=TimeInd/fs;


%%for breaks...
if Timer > cutoff
    pause_dex= pause_dex+ 1/fs;
end

if pause_dex == breaks + Break_time + 1
    pause_dex= 1/fs;
end

if pause_dex== breaks
    pause_game()
end
if pause_dex == breaks + Break_time
    close_pause()
end

%amplitude_mat1(:,dex1)=[block.InputPort(1).Data(1) ; block.InputPort(1).Data(2), block.InputPort(1).Data(3), block.InputPort(1).Data(4), block.InputPort(1).Data(5),block.InputPort(1).Data(6),block.InputPort(1).Data(7),block.InputPort(1).Data(8), block.InputPort(1).Data(9),block.InputPort(1).Data(10), block.InputPort(1).Data(11), block.InputPort(1).Data(12)];
% dex1= dex1+1;
% if dex1 ==round(fs*Sample_time) % alows us to take the averge power band of the folowing 64 samples (65/256=0.25- every 0.25 seconds)
%     dex1=1;
% end

%start the game
if actime == start_time
    d = createMessage(-200,1);
    fwrite(u,d);
    fwrite(u2,d);
end

%finish the game
if actime ==start_time + Session_Time + N_breaks*Break_time - 1
    d = createMessage(-400,1);
    fwrite(u,d);
    fwrite(u2,d);
end

%start calculating p300 after sound
if Timer_on==1;
    timer2= timer2 +1;
    
    amplitude_mat1(:,timer2)=[block.InputPort(1).Data(1) ; block.InputPort(1).Data(2); block.InputPort(1).Data(3); block.InputPort(1).Data(4); block.InputPort(1).Data(5); block.InputPort(1).Data(6); block.InputPort(1).Data(7); block.InputPort(1).Data(8); block.InputPort(1).Data(9);block.InputPort(1).Data(10)];
   
    %if we add eyes to the analysis:
    %amplitude_mat1(:,timer2)=[block.InputPort(1).Data(1) ; block.InputPort(1).Data(2); block.InputPort(1).Data(3); block.InputPort(1).Data(4); block.InputPort(1).Data(5); block.InputPort(1).Data(6); block.InputPort(1).Data(7); block.InputPort(1).Data(8); block.InputPort(1).Data(9);block.InputPort(1).Data(10); block.InputPort(1).Data(11); block.InputPort(1).Data(12); block.InputPort(1).Data(13) ; block.InputPort(1).Data(14)];
end

%send the sounds and the surprise parameter
if artifact_on==0 && Break_on==0
    
    
    if (actime>start_time) && (mod(TimeInd,round(fs*Sound_time))==0)
        
        % the sound vector
        
        Sound_type=soundSequence(Scount);
        
        
        if Sound_type==1 %low sound
            % %           beep = allBeeps(2,:);
            % trigger=10;
            UnitySound= -44444; % typecast(-44444, 'uint8')
        elseif Sound_type==0
% %             beep = allBeeps(1,:);
         % trigger=20;
           UnitySound= -55555; %high sound
        end
                      
% % %         Priority(1);
% % %         PsychPortAudio('FillBuffer', pahandle, beep);
        
%         t0 = GetSecs;
% % %         PsychPortAudio('Start', pahandle,[],[],1);
% % %         outp(address,trigger);
% % %         WaitSecs(1/256); % wait the time of the frequency sample
% % %         outp(address,0);        
% % %         PsychPortAudio('Stop', pahandle, 1);        
%         stopTime = GetSecs;% record the sound time       
%         GetSecs - t0
%         sound(beep,44100);
  
        timer3=TimeInd;
        d = createMessage(UnitySound,1);
        fwrite(u,d);
        fwrite(u2,d);
        Timer_on=1; %start the timer for calculating the p300
        
    end
    
%     if timer3== TimeInd + 1 % ?????????
%      outp(address,0); % make the trigger 0 again
%     end
    
    %send the brain parmeter (every 1.2 seconds)
    if (actime>start_time) && (mod(timer2,round(fs* Sample_time))==0) && (timer2>0)
                
        currFeature = calc_eeg_feature_online(amplitude_mat1,featureInfo);                        
        
        featureVec = [featureVec currFeature];
        %handles.userdata.featureVec = featureVec;
        
        if Scount<minTrialsNumforNF
            
            NF_param=0; %correlation between -1 to 1
            disp('Surprise     feature')
            str = sprintf('%0.3f     %0.3f',surprise(Scount),featureVec(end));
            disp(str);
        else
            counter= counter + 1;
            NF_param=calc_NF_param(featureVec,surprise(1:Scount)',pastLength); %correlation between -1 to 1            
            NFparamForGraph(counter)= NF_param;
            disp('Surprise     feature     NFparam')
            disp([surprise(Scount) , featureVec(end), NF_param])
        end
        %movment of the car only above correlation of 0.1 (remind Hadar to make featureInfo.corrThresh
        if NF_param< featureInfo.corrThresh
            val_unity= 0.1;
        else
            val_unity= NF_param;
        end
        
        %sending the values to unity:
        d = createMessage(val_unity,1);
        fwrite(u,d);
        fwrite(u2,d);
        
        if val==1
            counter_right= counter_right +1;
        end
        
%         figure(20)
%         plot(
        
        %reset parameter
        amplitude_mat1= zeros(Elec_num,round(fs* Sample_time));
        Scount=Scount+1;
        timer2=0;
        Timer_on=0;
        disp('trial')
        disp(Scount)
        disp('NF parameter')
        disp(NF_param)
        
        
    end
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%CONTINUE BUTTON

% --- Executes on button press in pushbuttonContinue.
function pushbuttonContinue_Callback(hObject, eventdata, handles)
% hObject    handle to pushbuttonContinue (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global modelName


set(handles.pushbuttonContinue,'enable','off')
set(handles.pushbuttonPause,'enable','on')
set_param(modelName,'SimulationCommand','continue')
pause(5)

%PAUSE BUTTON

% --- Executes on button press in pushbuttonPause.
function pushbuttonPause_Callback(hObject, eventdata, handles)
% hObject    handle to pushbuttonPause (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global modelName

set(handles.pushbuttonPause,'enable','off')
set(handles.pushbuttonContinue,'enable','on')

set_param(modelName,'SimulationCommand','pause')


%SOUND BUTTON

% --- Executes on button press in radiobutton1.
function radiobutton1_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global sound_on
%global set_sound

sound_on= get(hObject,'Value');%returns toggle state of radiobutton1

if sound_on==1
    disp('sound is on')
else
    disp('sound is off')
end






% --- If Enable == 'on', executes on mouse press in 5 pixel border.
% --- Otherwise, executes on mouse press in 5 pixel border or over pushbuttonPause.
function pushbuttonPause_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to pushbuttonPause (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
